import os
from distutils.command.build import build

from django.core import management
from setuptools import setup, find_packages

try:
    with open(
        os.path.join(os.path.dirname(__file__), "README.rst"), encoding="utf-8"
    ) as f:
        long_description = f.read()
except FileNotFoundError:
    long_description = ""


class CustomBuild(build):
    def run(self):
        management.call_command("compilemessages", verbosity=1)
        build.run(self)


cmdclass = {"build": CustomBuild}


setup(
    name="pretix_via",
    version="1.0.0",
    description="Study association via membership checker for pretix",
    long_description=long_description,
    url="GitHub repository URL",
    author="Maico Timmerman",
    author_email="maico.timmerman@gmail.com",
    license="Apache Software License",
    install_requires=["django", "djangorestframework", "django-filter", "requests"],
    packages=find_packages(exclude=["tests", "tests.*"]),
    include_package_data=True,
    cmdclass=cmdclass,
    entry_points="""
[pretix.plugin]
pretix_via=pretix_via:PretixPluginMeta
""",
)
