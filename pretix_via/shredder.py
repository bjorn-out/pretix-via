from django.db import transaction
from django.utils.translation import gettext_lazy as _

from pretix.base.models import Order, WaitingListEntry
from pretix.base.shredder import BaseDataShredder, WaitingListShredder


class OrderViaUserShredder(BaseDataShredder):
    verbose_name = _("VIA Membership")
    identifier = "order_via_user"
    description = _("This will remove all via membership data from orders.")

    def generate_files(self) -> None:
        return None

    @transaction.atomic
    def shred_data(self):
        for o in self.event.orders.all():
            try:
                o.via_user.email = "█"
                o.via_user.address = "█"
                o.via_user.city = "█"
                o.via_user.zip = "█"
                o.via_user.favourer = False
                o.via_user.member = False
                o.via_user.first_name = "█"
                o.via_user.last_name = "█"
                o.via_user.save()
            except Order.via_user.RelatedObjectDoesNotExist:
                pass


class WaitinglistViaUserShredder(WaitingListShredder):
    @transaction.atomic
    def shred_data(self):
        super(WaitinglistViaUserShredder, self).shred_data()

        for w in self.event.waitinglistentries.all():
            try:
                w.via_user.email = "█"
                w.via_user.address = "█"
                w.via_user.city = "█"
                w.via_user.zip = "█"
                w.via_user.favourer = False
                w.via_user.member = False
                w.via_user.first_name = "█"
                w.via_user.last_name = "█"
                w.via_user.save()
            except WaitingListEntry.via_user.RelatedObjectDoesNotExist:
                pass
