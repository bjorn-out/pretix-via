import logging

from django.utils.translation import gettext_lazy

from pretix_via.settings import DEFAULTS

try:
    from pretix.base.plugins import PluginConfig
except ImportError:
    raise RuntimeError("Please use pretix 2.7 or above to run this plugin!")

logger = logging.getLogger(__name__)


class PluginApp(PluginConfig):
    name = "pretix_via"
    verbose_name = "Pretix for via"

    class PretixPluginMeta:
        name = gettext_lazy("Pretix for via")
        author = "Maico Timmerman"
        description = gettext_lazy(
            "Study association via membership checker for pretix"
        )
        visible = True
        version = "1.0.0"
        # compatibility = "pretix>=2.7.0"

    def ready(self):
        from .signals import (  # noqa: F401
            checkout,
            live_issues,
            navigation,
            settings,
            shredder,
            dashboard,
            exporters,
        )
        from pretix.base.settings import settings_hierarkey
        from .settings import NEW_MAIL_DEFAULTS

        # Based on pretix.base.settings line 851.
        for k, v in NEW_MAIL_DEFAULTS.items():
            settings_hierarkey.add_default(k, v["default"], v["type"])
            logger.info(f"Overwritten '{k}' default mail setting.")

        for k, v in DEFAULTS.items():
            settings_hierarkey.add_default(k, v["default"], v["type"])
            logger.info(f"Adding '{k}' default setting.")

        from pretix.control.views.main import EventWizard

        assert (
            EventWizard.templates["foundation"]
            == "pretixcontrol/events/create_foundation.html"
        ), "Original template expected at pretixcontrol/events/create_foundation.html"
        EventWizard.templates["foundation"] = "pretix_via/create_foundation.html"


logger.info("Loaded pretix_via configuarions")
default_app_config = "pretix_via.PluginApp"
