import json
from decimal import Decimal

from compat import DjangoJSONEncoder
from django import forms
from django.utils.translation import gettext_lazy as _
from i18nfield.forms import I18nForm, I18nFormSetMixin, I18nFormField, I18nTextInput

from pretix.base.forms import SettingsForm
from pretix.base.models import Question
from pretix.base.reldate import RelativeDateWrapper, RelativeDateTimeField
from pretix.base.settings import settings_hierarkey
from pretix.helpers.money import DecimalTextInput, change_decimal_field


class EventViaSettingsForm(SettingsForm):
    via_membership_required = forms.BooleanField(
        label=_("VIA Membership required"),
        help_text=_(
            "Orders can be placed only when the ordering "
            "authentication code is related to a user on the "
            "via-website which is a member"
        ),
        required=False,
    )

    via_favourer_allowed = forms.BooleanField(
        label=_("VIA favourer allowed"),
        help_text=_(
            "Orders can be placed only when the ordering "
            "authentication code is related to a user on the "
            "via-website which is a favourer"
        ),
        required=False,
    )


settings_hierarkey.add_default("availability_quotas", [], list)
settings_hierarkey.add_default("availability_reserved", 0, int)
settings_hierarkey.add_default("replacement_until", None, RelativeDateWrapper)
settings_hierarkey.add_default("no_show_fee", "0.00", Decimal)
settings_hierarkey.add_default("questions_terms", "{}", str)
settings_hierarkey.add_default("additional_terms", [], list)


class EventViaTermsForm(SettingsForm):
    availability_quotas = forms.MultipleChoiceField(
        label=_("Availability quotas"),
        required=True,
        choices=[],
        widget=forms.CheckboxSelectMultiple,
        help_text=_("Used to calculate the initial number of available tickets."),
    )
    availability_reserved = forms.IntegerField(
        label=_("Reserved places"),
        required=True,
        min_value=0,
        initial=0,
        help_text=_(
            "Subtracted from the initial number of available tickets. Use for reserved tickets of committee members."
        ),
    )
    replacement_until = RelativeDateTimeField(
        label=_("Do not allow replacements after"),
        required=False,
        help_text=_(
            "Deadline to put someone forward as a replacement for your registration. "
            "Not set will not allow replacements, and remove the clause from the terms and condition."
        ),
    )
    no_show_fee = forms.DecimalField(
        required=False,
        label=_("No show fee"),
        min_value=Decimal("0.00"),
        widget=DecimalTextInput(places=2),
        help_text=_(
            "The costs that are made for the participation, on top of the price of the ticket. "
            "A no-show fee of 0.00 will hide the clause from the terms and conditions"
        ),
    )

    has_subactivities = forms.BooleanField(
        required=False,
        label=_("Event has subactivities"),
        help_text=_(
            "An additional clause is added for mandatory attendence to subactivities of this event."
        ),
    )

    has_transportation = forms.BooleanField(
        required=False,
        label=_("Event has transportation"),
        help_text=_("An additional clause is added in case transport is missed."),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.event = kwargs.pop("obj", None)
        self.fields["availability_quotas"].initial = [
            int(s) for s in self.event.settings.availability_quotas
        ]
        self.fields["availability_quotas"].choices = [
            (c.id, c.name) for c in self.event.quotas.all()
        ]
        change_decimal_field(self.fields["no_show_fee"], self.event.currency)

    def clean(self):
        value = super().clean()

        availability_reserved = value.get("availability_reserved")
        availability_quotas = value.get("availability_quotas", [])
        if availability_quotas:
            quotas = self.event.quotas.filter(
                id__in=self.cleaned_data["availability_quotas"]
            ).all()
            s = 0
            for quota in quotas:
                # If an quota exists without a limit, unlimited tickets are available and we do not
                # need to check the quota.
                if quota.size is None:
                    return value
                else:
                    s += quota.size

            if s < availability_reserved:
                self.add_error(
                    "availability_reserved",
                    forms.ValidationError(
                        "Reserved places result in negative number of initial places available."
                    ),
                )
        return value


class BaseEventViaTermsQuestionsFormSet(I18nFormSetMixin, forms.BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop("obj", None)
        if self.event:
            kwargs["locales"] = self.event.settings.get("locales")
        super().__init__(*args, **kwargs)

    def save(self):
        questions_terms = {}
        for cleaned_data in self.cleaned_data:
            questions_terms[cleaned_data["question"].pk] = {
                "goal": cleaned_data["goal"].data,
                "externally_shared_with": cleaned_data["externally_shared_with"].data,
            }
        self.event.settings.questions_terms = json.dumps(
            questions_terms, cls=DjangoJSONEncoder
        )


class EventViaTermsQuestionsForm(I18nForm):
    question = forms.ModelChoiceField(
        queryset=Question.objects.none(), required=True, widget=forms.HiddenInput
    )
    goal = I18nFormField(
        label=_("Goal for saving"),
        help_text=_(
            "The goal of saving this personal data. (i.e. shirt size: to be able to order fitting clothing, phone number: contact the member"
        ),
        required=True,
        widget=I18nTextInput,
    )
    externally_shared_with = I18nFormField(
        label=_("Externally shared with"),
        help_text=_(
            "Comma-seperated list of third parties with whom this personal data is shared."
            "Not applicable for cumulatives (i.e. total number of shirts of specific size or total number of vegans)"
        ),
        required=False,
        widget=I18nTextInput,
    )

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop("obj", None)
        self.locales = (
            self.event.settings.get("locales")
            if self.event
            else kwargs.pop("locales", None)
        )
        kwargs["locales"] = self.locales
        super().__init__(*args, **kwargs)

        # Initial all fields of this form.
        self.fields["question"].queryset = self.event.questions.all()


class EventViaTermsAdditionalForm(I18nForm):
    term = I18nFormField(
        label=_("Additional terms and conditions"), required=True, widget=I18nTextInput
    )

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop("obj", None)
        self.locales = (
            self.event.settings.get("locales")
            if self.event
            else kwargs.pop("locales", None)
        )
        kwargs["locales"] = self.locales
        super().__init__(*args, **kwargs)


class BaseEventViaTermsAdditionalFormSet(I18nFormSetMixin, forms.BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop("obj", None)
        if self.event:
            kwargs["locales"] = self.event.settings.get("locales")
        super().__init__(*args, **kwargs)

    def save(self):
        additional_terms = sorted(self.cleaned_data, key=lambda x: x["ORDER"])
        additional_terms = [
            json.dumps(t["term"].data) for t in additional_terms if not t["DELETE"]
        ]
        self.event.settings.additional_terms = additional_terms
