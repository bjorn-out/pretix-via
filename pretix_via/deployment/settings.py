EMAIL_BACKEND = "pretix_via.smtp.SMTPRelayFixEmailBackend"
EMAIL_FQDN = config.get("mail", "fqdn", fallback="svia.nl")  # noqa[F821]
