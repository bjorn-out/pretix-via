from collections import OrderedDict

from django import forms
from django.dispatch import receiver
from django.utils.translation import gettext_lazy

from pretix.base.exporter import ListExporter
from pretix.base.models import OrderPosition, Order
from pretix.base.signals import register_data_exporters


class HeliosExporter(ListExporter):
    name = "overview"
    identifier = "helios"
    verbose_name = gettext_lazy("Helios export")

    @property
    def additional_form_fields(self):
        return OrderedDict(
            [
                (
                    "authorizing",
                    forms.ModelChoiceField(
                        queryset=self.event.items.all(),
                        label=gettext_lazy("Authorizing product"),
                        widget=forms.RadioSelect(attrs={"class": "scrolling-choice"}),
                        initial=self.event.items.last(),
                    ),
                ),
                (
                    "authorizing_name",
                    forms.ModelChoiceField(
                        queryset=self.event.questions.all(),
                        label=gettext_lazy("Authorizing name"),
                        widget=forms.RadioSelect(attrs={"class": "scrolling-choice"}),
                        initial=self.event.questions.first(),
                    ),
                ),
                (
                    "authorizing_email",
                    forms.ModelChoiceField(
                        queryset=self.event.questions.all(),
                        label=gettext_lazy("Authorizing e-mail"),
                        widget=forms.RadioSelect(attrs={"class": "scrolling-choice"}),
                        initial=self.event.questions.last(),
                    ),
                ),
            ]
        )

    def iterate_list(self, form_data):
        qs = (
            OrderPosition.objects.filter(order__event=self.event)
            .prefetch_related(
                "answers",
            )
            .select_related(
                "order",
                "item",
            )
        )

        qs = qs.filter(order__status=Order.STATUS_PAID)

        yield self.ProgressSetTotal(total=qs.count())

        for op in qs:
            if op.item_id == form_data["authorizing"]:
                answers = {a.question_id: a.answer for a in op.answers.all()}
                authorizing_name = answers.get(form_data["authorizing_name"], "UNKNOWN")
                authorizing_email = answers.get(
                    form_data["authorizing_email"], "UNKNOWN"
                )
                yield [
                    op.order.code,
                    authorizing_email,
                    f"{authorizing_name} authorized by {op.attendee_name}",
                ]
                continue
            else:
                yield [
                    op.order.code,
                    op.attendee_email or op.order.email or "",
                    op.attendee_name,
                ]

    def get_filename(self):
        return f"helios_export_{self.event.slug}"


@receiver(
    register_data_exporters, dispatch_uid="pretix_via_register_data_exporters_helios"
)
def register_data_exporters_helios(sender, *args, **kwargs):
    return HeliosExporter
