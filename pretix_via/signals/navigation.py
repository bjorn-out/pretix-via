from django.dispatch import receiver
from django.urls import resolve, reverse
from django.utils.translation import ugettext_lazy as _

from pretix.control.signals import nav_event_settings
from pretix.multidomain.urlreverse import eventreverse
from pretix.presale.signals import footer_link


@receiver(nav_event_settings, dispatch_uid="pretix_via_control_event_settings")
def navbar_settings(sender, request, **kwargs):
    url = resolve(request.path_info)
    return [
        {
            "label": _("VIA Membership"),
            "url": reverse(
                "plugins:pretix_via:event.settings.via",
                kwargs={
                    "event": request.event.slug,
                    "organizer": request.organizer.slug,
                },
            ),
            "active": url.namespace == "plugins:pretix_via"
            and url.url_name == "event.settings.via",
        },
        {
            "label": _("VIA Terms"),
            "url": reverse(
                "plugins:pretix_via:event.settings.via_terms",
                kwargs={
                    "event": request.event.slug,
                    "organizer": request.organizer.slug,
                },
            ),
            "active": url.namespace == "plugins:pretix_via"
            and url.url_name == "event.settings.via_terms",
        },
    ]


@receiver(footer_link, dispatch_uid="pretix_via_footer_links")
def footer_link_pages(sender, request=None, **kwargs):
    if not sender.settings.via_terms_enabled:
        return []

    return [
        {
            "label": _("Terms and conditions"),
            "url": eventreverse(sender, "plugins:pretix_via:terms"),
        }
    ]
