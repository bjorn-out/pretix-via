from decimal import Decimal

from django.conf import settings
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from pretix_pages.models import Page

from pretix.base.models import Event
from pretix.base.signals import event_live_issues
from pretix.multidomain.urlreverse import eventreverse


# TODO Dutch and english question options check.
@receiver(
    event_live_issues, dispatch_uid="pretix_via_event_live_issues_english_enabled"
)
def check_event_live_issue_english_enabled(sender: Event, **kwargs):
    if "en" not in sender.settings.locales:
        return _(
            "Event needs to support the English language, please enable it {here}."
        ).format(
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings") + "#tab-0-1-open",
                here=_("here"),
            ),
        )


@receiver(event_live_issues, dispatch_uid="pretix_via_event_live_issues_dutch_enabled")
def check_event_live_issue_dutch_enabled(sender: Event, **kwargs):
    if "nl" not in sender.settings.locales:
        return _(
            "Event needs to support the Dutch language, please enable it {here}."
        ).format(
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings") + "#tab-0-1-open",
                here=_("here"),
            ),
        )


@receiver(
    event_live_issues, dispatch_uid="pretix_via_event_live_issues_default_mail_reply"
)
def check_event_live_issue_default_mail_reply(sender: Event, **kwargs):
    if sender.settings.mail_from == settings.DEFAULT_FROM_EMAIL:
        return _(
            "The reply e-mailadress is still set to the default ({default_from}),"
            " please change to your committee email {here}."
        ).format(
            default_from=settings.DEFAULT_FROM_EMAIL,
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings.mail"), here=_("here")
            ),
        )


@receiver(event_live_issues, dispatch_uid="pretix_via_event_live_issues_english_title")
def check_event_live_issue_english_title(sender: Event, **kwargs):
    if not sender.name.data.get("en"):
        return _("Event needs to have a English name, please set it {here}.").format(
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings"), here=_("here")
            ),
        )


@receiver(event_live_issues, dispatch_uid="pretix_via_event_live_issues_dutch_title")
def check_event_live_issue_dutch_title(sender: Event, **kwargs):
    if not sender.name.data.get("nl"):
        return _("Event needs to have a Dutch name, please set it {here}.").format(
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings"), here=_("here")
            ),
        )


@receiver(
    event_live_issues, dispatch_uid="pretix_via_event_live_issues_english_product_title"
)
def check_event_live_issue_english_product_title(sender: Event, **kwargs):
    items = [
        str(item.name) for item in sender.items.all() if not item.name.data.get("en")
    ]
    if items:
        return _(
            "Product(s) '%s' needs to have a English name, please set them {here}."
        ).format(
            names=", ".join(item for item in items),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.items"), here=_("here")
            ),
        )


@receiver(
    event_live_issues, dispatch_uid="pretix_via_event_live_issues_dutch_product_title"
)
def check_event_live_issue_dutch_product_title(sender: Event, **kwargs):
    items = [
        str(item.name) for item in sender.items.all() if not item.name.data.get("nl")
    ]
    if items:
        return _(
            "Product(s) '{names}' needs to have a Dutch name, please set them {here}."
        ).format(
            names=", ".join(item for item in items),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.items"), here=_("here")
            ),
        )


@receiver(event_live_issues, dispatch_uid="pretix_via_event_live_issues_vat_assigned")
def check_event_live_issue_vat_assigned(sender: Event, **kwargs):
    items = []
    for item in sender.items.all():
        if item.tax_rule:
            continue
        if item.default_price is None or item.default_price == Decimal("0.00"):
            continue

        items.append(str(item.name))
    if items:
        return _("Product(s) '{names}' are missing VAT tax rules").format(
            names=",".join(item for item in items)
        )


@receiver(event_live_issues, dispatch_uid="pretix_via_event_live_issues_vat_values")
def check_event_live_issue_vat_values(sender: Event, **kwargs):
    items = []
    valid_percentages = {Decimal("21.00"), Decimal("9.00"), Decimal("0.00")}
    for tax_rule in sender.tax_rules.all():
        if tax_rule.rate not in valid_percentages:
            items.append(tax_rule)

    if items:
        return _(
            "Tax rule(s) '{names}' have invalid percentages, 0%, 9%, and 21% are allowed. Change them {here}."
        ).format(
            names=",".join(str(item) for item in items),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings.tax"), here=_("here")
            ),
        )


@receiver(event_live_issues, dispatch_uid="pretix_via_event_live_issues_vat_name")
def check_event_live_issue_vat_name(sender: Event, **kwargs):
    items = []

    for t in sender.tax_rules.all():
        if (
            t.name.data.get("nl").lower() != "btw"
            or t.name.data.get("en").lower() != "vat"
        ):
            items.append(t)

    if items:
        return _(
            "Tax rule(s) '{names}' have invalid naming, "
            "please name the tax rule 'VAT' (en), 'Btw' (nl). Change them {here}."
        ).format(
            names=",".join(str(item) for item in items),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings.tax"), here=_("here")
            ),
        )


@receiver(event_live_issues, dispatch_uid="pretix_via_event_live_issues_vat_exclusive")
def check_event_live_issue_vat_exclusive(sender: Event, **kwargs):
    items = [t for t in sender.tax_rules.all() if not t.price_includes_tax]

    if items:
        return _(
            "Tax rule(s) '{names}' have price excluding tax, "
            "please use tax rules that require product's prices to be specified including VAT. "
            "Change them {here}"
        ).format(
            names=",".join(str(item) for item in items),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings.tax"), here=_("here")
            ),
        )


@receiver(
    event_live_issues, dispatch_uid="pretix_via_event_live_issues_payment_providers"
)
def check_event_live_issue_payment_providers(sender: Event, **kwargs):
    allowed = {"mollie_ideal", "mollie_bancontact", "free", "banktransfer"}
    allowed_implicit = {"offsetting", "boxoffice", "banktransfer"}
    available = sender.get_payment_providers()
    enabled = set(p.identifier for p in available.values() if p.is_enabled)

    need_disabling = enabled - allowed.union(allowed_implicit)
    if need_disabling:
        allowed_names = []
        for method in allowed:
            if method in available:
                allowed_names.append(str(available[method].verbose_name))

        return _(
            "VIA currently does not support the following payment methods: '{unsupported_payment_methods}'."
            " Allowed methods are: '{allowed_payment_methods}'. Please disable other payment providers {here}."
        ).format(
            unsupported_payment_methods=", ".join(
                str(sender.get_payment_providers()[method].verbose_name)
                for method in need_disabling
            ),
            allowed_payment_methods=", ".join(allowed_names),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.settings.payment"),
                here=_("here"),
            ),
        )


@receiver(
    event_live_issues, dispatch_uid="pretix_via_event_live_issues_dutch_question_title"
)
def check_event_live_issue_dutch_question_title(sender: Event, **kwargs):
    items = [
        str(item.question)
        for item in sender.questions.all()
        if not item.question.data.get("nl")
    ]
    if items:
        return _(
            "Questions '{names}' need to have a Dutch title, please set them {here}."
        ).format(
            names=", ".join(item for item in items),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.items.questions"),
                here=_("here"),
            ),
        )


@receiver(
    event_live_issues,
    dispatch_uid="pretix_via_event_live_issues_english_question_title",
)
def check_event_live_issue_english_question_title(sender: Event, **kwargs):
    items = [
        str(item.question)
        for item in sender.questions.all()
        if not item.question.data.get("en")
    ]
    if items:
        return _(
            "Questions '{names}' need to have a English title, please set them {here}."
        ).format(
            names=", ".join(item for item in items),
            here="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "control:event.items.questions"),
                here=_("here"),
            ),
        )


@receiver(
    event_live_issues, dispatch_uid="pretix_via_event_live_issues_terms_conditions"
)
def check_event_live_issues_terms_conditions(sender: Event, **kwargs):
    if sender.settings.via_terms_enabled:
        return

    check_url = "terms-and-conditions"
    try:
        page = Page.objects.get(event=sender, slug=check_url)
        page_edit_html = "Please change the page <a href='{url}'>{here}</a>".format(
            url=eventreverse(
                sender, "plugins:pretix_pages:edit", kwargs={"page": page.id}
            ),
            here=_("here"),
        )
        en_title, nl_title = "Terms and Conditions", "Algemene voorwaarden"
        if not page.title.data.get("en") == en_title:
            return _(
                "The page with url '{url}' exists, however English title should be '{title}'. {change}."
            ).format(
                url=check_url,
                change=page_edit_html,
                title=en_title,
            )
        if not page.title.data.get("nl") == nl_title:
            return _(
                "The page with url '{url}' exists, however Dutch title should be '{title}'. {change}."
            ).format(url=check_url, change=page_edit_html, title=nl_title)
        if not page.link_in_footer:
            return _(
                "The page with url '{url}' exists, however 'Show link in the event footer' should be enabled. {change}."
            ).format(
                url=check_url,
                change=page_edit_html,
            )
        if not page.require_confirmation:
            return _(
                "The page with url '{url}' exists, however it does not require acknowledgement. {change}."
            ).format(
                url=check_url,
                change=page_edit_html,
            )

    except Page.DoesNotExist:
        return _(
            "This event is missing Terms and Conditions, please configure terms {here1}. "
            "Alternatively, you can create a page {here2} with the URL '{check_url}' exactly."
        ).format(
            here1="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "plugins:pretix_via:event.settings.via_terms"),
                here=_("here"),
            ),
            here2="<a href='{url}'>{here}</a>".format(
                url=eventreverse(sender, "plugins:pretix_pages:index"), here=_("here")
            ),
            check_url=check_url,
        )
