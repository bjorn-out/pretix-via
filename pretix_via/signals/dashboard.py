from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from pretix.control.signals import event_dashboard_widgets


@receiver(signal=event_dashboard_widgets)
def live_issues_present(sender, **kwargs):
    return [
        {
            "display_size": "small",
            "priority": 999,
            "content": '<div class="shopstate">{t1}<br><span class="{cls}"><span class="fa {icon}"></span> {state}</span>{t2}</div>'.format(
                t1=_("Your ticket shop has"),
                t2=_("Click here to view"),
                state=_("live issues") if sender.live_issues else _("no live issues"),
                icon="fa-times-circle" if sender.live_issues else "fa-check-circle",
                # Copy the classes from the live dashboard widget for the colors.
                cls="off" if sender.live_issues else "live",
            ),
            "url": reverse(
                "control:event.live",
                kwargs={"event": sender.slug, "organizer": sender.organizer.slug},
            ),
        }
    ]
