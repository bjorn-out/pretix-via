import logging
from urllib.parse import urljoin

import requests
from django.contrib import messages
from django.db import IntegrityError, transaction
from django.utils.translation import gettext_lazy as _

from pretix.base.auth import BaseAuthBackend
from pretix.base.models import User, Team, Organizer
from pretix.base.settings import GlobalSettingsObject
from pretix_via.models import TeamViaGroup, UserViaUser

_logger = logging.getLogger(__name__)


def check_settings_configured(f):
    def wrapper(backend, request):
        if not backend.organizer or not backend.client_id or not backend.client_secret:
            messages.error(
                request,
                "Global settings for user provisioning not set. "
                "Please configure settings before logging in.",
            )
            return None
        return f(backend, request)

    return wrapper


class ViaOAuthBackend(BaseAuthBackend):
    identifier = "via-oauth"
    verbose_name = _("Login with via")

    def __init__(self) -> None:
        super().__init__()

        # Load the organizer used provisioning user groups.
        gs = GlobalSettingsObject()
        if gs.settings.via_oauth_organizer:
            self.organizer = Organizer.objects.get(pk=gs.settings.via_oauth_organizer)
        else:
            self.organizer = None

        self.client_id = gs.settings.get("via_oauth_client_id")
        self.client_secret = gs.settings.get("via_oauth_client_secret")
        self.host = gs.settings.get("via_oauth_host", "https://svia.nl/")

    def authentication_url(self, request):
        state = request.GET.get("next", "")
        return urljoin(
            self.host,
            "/oauth/authorize"
            "?response_type=code"
            f"&client_id={self.client_id}"
            f"&redirect_uri={request.build_absolute_uri('')}"
            "&scope=user group"
            f"&state={state}",
        )

    @check_settings_configured
    def request_authenticate(self, request):
        authorization_code = request.GET.get("code")
        if not authorization_code:
            return

        token_data = {
            "code": authorization_code,
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "redirect_uri": request.build_absolute_uri(""),
            "grant_type": "authorization_code",
            "scope": "user group",
        }
        token_url = urljoin(self.host, "/oauth/token")

        response = requests.request("POST", token_url, data=token_data)
        if response.status_code != 200:
            messages.error(
                request, _("Login with via failed. No valid authorization code.")
            )
            return

        login_response = response.json()

        access_token = login_response.get("access_token")

        # Check the actual user id in viaduct using OAuth token introspection.
        introspect_url = urljoin(self.host, "/oauth/introspect")
        introspect_data = {
            "token": access_token,
            "client_id": self.client_id,
            "client_secret": self.client_secret,
        }
        response = requests.request("POST", introspect_url, data=introspect_data)
        if response.status_code != 200:
            messages.error(
                request, _("Login with via failed. Failed retrieving user information.")
            )
            return

        # 1. Retrieve existing user from viaduct
        introspect_response = response.json()
        user_via_id = introspect_response["sub"]
        try:
            user_via_user = UserViaUser.objects.get(user_via_id=user_via_id)
            user_via_user.user.fullname = introspect_response.get("full_name")
            user_via_user.user.email = introspect_response.get("username").lower()
            user_via_user.user.save()
        except UserViaUser.DoesNotExist:
            try:
                with transaction.atomic():
                    user = User(
                        email=introspect_response.get("username").lower(),
                        fullname=introspect_response.get("full_name"),
                        auth_backend=self.identifier,
                    )
                    user.save()
                    user_via_user = UserViaUser(user=user, user_via_id=user_via_id)
                    user_via_user.save()
                    messages.info(
                        request, _("An account was not found, one has been provisioned")
                    )
            except IntegrityError:
                messages.error(
                    request,
                    _(
                        "An account with this e-mail exists, however it has no linked via user details."
                        " Please contact administrator."
                    ),
                )
                return None

        # 3. Get correct groups for the user.
        group_url = urljoin(self.host, "/api/users/self/groups")
        response = requests.request(
            "GET", group_url, headers={"Authorization": f"Bearer {access_token}"}
        )
        if response.status_code != 200:
            messages.warning(request, _("Unable to provision user's via groups."))
            self.clear_user_via_groups(user_via_user.user)
            return user_via_user.user

        # 4. Remove all via groups and add user to current groups.
        groups_reponse = response.json()
        self.clear_user_via_groups(user_via_user.user)
        for group in groups_reponse:
            mail = None
            if group["maillist"]:
                mail = f"{group['maillist']}@svia.nl"
            try:
                team_via_group = TeamViaGroup.objects.get(group_id=group["id"])
            except TeamViaGroup.DoesNotExist:
                team, team_created = Team.objects.get_or_create(
                    organizer=self.organizer,
                    name=group["name"],
                    can_create_events=True,
                    can_change_event_settings=True,
                    can_change_items=True,
                    can_view_orders=True,
                    can_change_orders=True,
                    can_view_vouchers=True,
                    can_change_vouchers=True,
                )
                team_via_group = TeamViaGroup.objects.create(
                    team=team, group_id=group["id"], email=mail
                )

            team_via_group.team.members.add(user_via_user.user)
            # Name of the via group might have changed, so force update.
            team_via_group.team.name = group["name"]
            team_via_group.team.save()
            team_via_group.email = mail
            team_via_group.save()

        return user_via_user.user

    def clear_user_via_groups(self, user: User):
        user.teams.remove(*user.teams.filter(organizer=self.organizer))

    def get_next_url(self, request):
        return request.GET.get("state")
