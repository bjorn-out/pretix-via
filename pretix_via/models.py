from django.db import models
from django.utils.translation import gettext_lazy as _


class ViaUser(models.Model):
    user_id = models.IntegerField(verbose_name=_("VIA UserID"))
    email = models.CharField(max_length=256, verbose_name=_("E-mail"))
    address = models.CharField(max_length=256, verbose_name=_("User address"))
    city = models.CharField(max_length=256, verbose_name=_("User city"))
    zip = models.CharField(max_length=256, verbose_name=_("User zip"))
    member = models.BooleanField(verbose_name=_("Member"))
    favourer = models.BooleanField(verbose_name=_("Favourer"))
    first_name = models.CharField(max_length=256, verbose_name=_("First name"))
    last_name = models.CharField(max_length=256, verbose_name=_("Last name"))

    class Meta:
        abstract = True


class OrderViaUser(ViaUser):
    order = models.OneToOneField(
        "pretixbase.Order", related_name="via_user", on_delete=models.CASCADE
    )


class WaitingListEntryViaUser(ViaUser):
    waitinglistentry = models.OneToOneField(
        "pretixbase.WaitingListEntry", related_name="via_user", on_delete=models.CASCADE
    )


class UserViaUser(models.Model):
    user = models.OneToOneField(
        "pretixbase.User", related_name="via_user", on_delete=models.CASCADE
    )
    user_via_id = models.IntegerField(verbose_name=_("VIA UserId"), unique=True)


class TeamViaGroup(models.Model):
    team = models.OneToOneField(
        "pretixbase.Team", related_name="via_group", on_delete=models.CASCADE
    )
    group_id = models.IntegerField(verbose_name=_("VIA GroupID"), unique=True)
    email = models.EmailField(verbose_name=_("E-mail address"), null=True)
