import datetime
import re
from typing import Union

import requests_mock
from bs4 import BeautifulSoup
from django.utils.timezone import now
from django_scopes import scopes_disabled
from django.test.testcases import TestCase

from pretix.base.models import (
    Organizer,
    Event,
    Quota,
    Item,
    CartPosition,
    Order,
    WaitingListEntry,
    Voucher,
)
from pretix.testutils.sessions import get_cart_session_key
from pretix_via.models import OrderViaUser, WaitingListEntryViaUser


class TestCheckoutViaPlugin(TestCase):
    @scopes_disabled()
    def setUp(self):
        super().setUp()
        self.orga = Organizer.objects.create(name="Studievereniging via", slug="via")
        self.event = Event.objects.create(
            organizer=self.orga,
            name="Borrel",
            slug="borrel",
            date_from=datetime.datetime(
                now().year + 1, 12, 26, tzinfo=datetime.timezone.utc
            ),
            plugins=",".join(
                [
                    "pretix.plugins.sendmail",
                    "pretix.plugins.statistics",
                    "pretix.plugins.checkinlists",
                    "pretix.plugins.autocheckin",
                    "pretix_via",
                    "pretix.plugins.ticketoutputpdf",
                    "pretix_pages",
                    "pretix.plugins.reports",
                    "pretix.plugins.pretixdroid",
                    "pretix_mollie",
                ]
            ),
            live=True,
        )
        self.tax_rule = self.event.tax_rules.create(rate=21)
        self.quota_tickets = Quota.objects.create(
            event=self.event, name="Tickets", size=5
        )
        self.ticket = Item.objects.create(
            event=self.event,
            name="Early-bird ticket",
            category=None,
            default_price=23,
            admission=True,
            tax_rule=self.tax_rule,
        )
        self.quota_tickets.items.add(self.ticket)
        self.event.settings.set("timezone", "UTC")
        self.event.settings.set("attendee_names_asked", False)
        self.client.get("/%s/%s/" % (self.orga.slug, self.event.slug))
        self.session_key = get_cart_session_key(self.client, self.event)

    def _set_session(self, key, value):
        session = self.client.session
        session["carts"][get_cart_session_key(self.client, self.event)][key] = value
        session.save()

    def _setup_mocker_svia_oauth(self, m: requests_mock.Mocker):
        m.post(
            "https://svia.nl/oauth/token?grant_type=client_credentials",
            json={
                "token_type": "Bearer",
                "access_token": "ACCESS_TOKEN",
                "expires_in": 864000,
                "scope": "pretix",
            },
        )

    def _setup_mocker_svia_user_info(self, m: requests_mock.Mocker):
        m.get(
            "https://svia.nl/api/pretix/users/SECRET_TOKEN",
            json={
                "id": 69,
                "email": "maico@gmail.com",
                "disabled": False,
                "first_name": "Maico",
                "last_name": "T",
                "student_id": "123456789",
                "birth_date": "2000-01-01",
                "phone_nr": "0612345678",
                "locale": "nl",
                "study_start": "2000-01-01",
                "educations": [
                    {
                        "id": 11,
                        "nl_name": "Artificial Intelligence",
                        "en_name": "Artificial Intelligence",
                        "programme_type": "master",
                    }
                ],
                "tfa_enabled": True,
                "address": "Science Park 904",
                "zip": "1098xh",
                "city": "Amsterdam",
                "country": "Nederland",
                "iban": "GB33BUKB20201555555555",
                "member": True,
                "paid_date": "2000-01-01",
                "member_of_merit_date": "2000-01-01",
                "favourer": False,
            },
        )

    def _setup_mocker_mollie_payment_create(self, m: requests_mock.Mocker):
        m.post(
            "https://api.mollie.com/v2/payments",
            json={
                "resource": "payment",
                "id": "tr_HQ7KU5bBd7",
                "mode": "test",
                "createdAt": "2018-10-04T12:05:57+00:00",
                "amount": {"value": "23.00", "currency": "EUR"},
                "description": "Upgrade via account",
                "method": "ideal",
                "metadata": {
                    "organizer": "via",
                    "event": "borrel",
                    "order": "EBBU1",
                    "payment": 1,
                },
                "status": "paid",
                "paidAt": "2018-10-04T12:06:15+00:00",
                "amountRefunded": {"value": "0.00", "currency": "EUR"},
                "amountRemaining": {"value": "35.00", "currency": "EUR"},
                "locale": "nl_NL",
                "countryCode": "NL",
                "profileId": "pfl_Qq54yD43me",
                "settlementId": "stl_yyrkGAxNS2",
                "sequenceType": "oneoff",
                "redirectUrl": "http://pretix.svia.nl/via/borrel/mollie/return/EBBU7/79a56308a5cb7ac9484cbc0cabc3b4e396314a06/1/",
                "webhookUrl": "http://pretix.svia.nl/via/borrel/mollie/webhook/1/",
                "settlementAmount": {"value": "23.00", "currency": "EUR"},
                "details": {
                    "consumerName": "J DOE",
                    "consumerAccount": "NL02ABNA0123456789",
                    "consumerBic": "ABNANL2A",
                },
                "_links": {
                    "self": {
                        "href": "https://api.mollie.com/v2/payments/tr_HQ7KU5bBd7",
                        "type": "application/hal+json",
                    },
                    "checkout": {
                        "href": "https://www.mollie.com/payscreen/select-method/7UhSN1zuXS",
                        "type": "text/html",
                    },
                    "settlement": {
                        "href": "https://api.mollie.com/v2/settlements/stl_yyrkGAxNS2",
                        "type": "application/hal+json",
                    },
                    "documentation": {
                        "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",
                        "type": "text/html",
                    },
                },
            },
        )

    def assert_via_user(self, via_user: Union[WaitingListEntryViaUser, OrderViaUser]):
        assert via_user.user_id == 69
        assert via_user.email == "maico@gmail.com"
        assert via_user.address == "Science Park 904"
        assert via_user.city == "Amsterdam"
        assert via_user.zip == "1098xh"
        assert via_user.member is True
        assert via_user.favourer is False
        assert via_user.first_name == "Maico"
        assert via_user.last_name == "T"

    def test_checkout_not_on_via_site(self):
        # No user info url is given using the widget data.
        with scopes_disabled():
            CartPosition.objects.create(
                event=self.event,
                cart_id=self.session_key,
                item=self.ticket,
                price=23,
                expires=now() - datetime.timedelta(minutes=10),
            )
        with requests_mock.Mocker():
            rv = self.client.get(f"/{self.orga.slug}/{self.event.slug}/checkout/start")
            self.assertRedirects(
                rv, f"/{self.orga.slug}/{self.event.slug}/checkout/viaduct/"
            )

            rv = self.client.get(
                f"/{self.orga.slug}/{self.event.slug}/checkout/viaduct/"
            )
            doc = BeautifulSoup(rv.content.decode(), "lxml")
            self.assertIn(
                "Unable to retrieve membership data. Please refresh the via-site and try again.",
                doc.select(".alert-danger")[0].text,
            )

    def do_checkout(self):
        rv = self.client.get(f"/{self.orga.slug}/{self.event.slug}/checkout/start")
        viaduct_step_url = f"/{self.orga.slug}/{self.event.slug}/checkout/viaduct/"
        self.assertRedirects(rv, viaduct_step_url)
        rv = self.client.post(viaduct_step_url, follow=True)
        questions_step_url = f"/{self.orga.slug}/{self.event.slug}/checkout/questions/"
        self.assertRedirects(rv, questions_step_url)
        rv = self.client.post(questions_step_url, follow=True)
        payment_step_url = f"/{self.orga.slug}/{self.event.slug}/checkout/payment/"
        self.assertRedirects(rv, payment_step_url)
        rv = self.client.post(
            payment_step_url, {"payment": "mollie_ideal"}, follow=True
        )
        confirm_step_url = f"/{self.orga.slug}/{self.event.slug}/checkout/confirm/"
        self.assertRedirects(rv, confirm_step_url)
        rv = self.client.post(confirm_step_url)
        assert rv.status_code == 302, rv.content
        self.assertRegex(
            rv.headers["location"],
            re.compile(r"/via/borrel/order/\w{5}/\w{16}/pay/\d/complete"),
        )

        rv = self.client.get(rv.headers["location"])
        assert rv.status_code == 302, rv.content
        self.assertRedirects(
            rv,
            "https://www.mollie.com/payscreen/select-method/7UhSN1zuXS",
            fetch_redirect_response=False,
        )

    def test_checkout(self):
        self._set_session(
            "widget_data",
            {"user-info-url": "https://svia.nl/api/pretix/users/SECRET_TOKEN"},
        )
        with requests_mock.Mocker() as m:
            self._setup_mocker_svia_oauth(m)
            self._setup_mocker_svia_user_info(m)
            self._setup_mocker_mollie_payment_create(m)

            with scopes_disabled():
                CartPosition.objects.create(
                    event=self.event,
                    cart_id=self.session_key,
                    item=self.ticket,
                    price=23,
                    expires=now() - datetime.timedelta(minutes=10),
                )

            self.do_checkout()

        # Validate the order contains via data.
        with scopes_disabled():
            o = Order.objects.first()
            via_user: OrderViaUser = o.via_user
            self.assert_via_user(via_user)

    def test_checkout_user_data_not_present_in_order(self):
        self.test_checkout()
        with scopes_disabled():
            o = Order.objects.first()
            assert "user_info" not in o.meta_info_data

    def test_waitinglist_not_on_via_site(self):
        self.quota_tickets.size = 0
        self.quota_tickets.save()

        with requests_mock.Mocker():
            rv = self.client.get(f"/{self.orga.slug}/{self.event.slug}/waitinglist")
            assert rv.status_code == 200

            rv = self.client.post(
                f"/{self.orga.slug}/{self.event.slug}/waitinglist",
                {
                    "email": "maico@gmail.com",
                    "itemvar": self.ticket.id,
                },
                follow=True,
            )
            self.assertRedirects(rv, f"/{self.orga.slug}/{self.event.slug}/")
            doc = BeautifulSoup(rv.content.decode(), "lxml")
            self.assertIn(
                "You can only use this shop on the via-site.",
                doc.select(".alert-danger")[0].text,
            )

    def test_waitinglist(self):
        self.quota_tickets.size = 0
        self.quota_tickets.save()
        self._set_session(
            "widget_data",
            {"user-info-url": "https://svia.nl/api/pretix/users/SECRET_TOKEN"},
        )
        with requests_mock.Mocker() as m:
            self._setup_mocker_svia_user_info(m)
            self._setup_mocker_svia_oauth(m)
            rv = self.client.get(f"/{self.orga.slug}/{self.event.slug}/waitinglist")
            assert rv.status_code == 200

            rv = self.client.post(
                f"/{self.orga.slug}/{self.event.slug}/waitinglist",
                {
                    "email": "maico@gmail.com",
                    "itemvar": self.ticket.id,
                },
            )
            assert rv.status_code == 302
            assert rv.headers["location"].startswith(
                f"/{self.orga.slug}/{self.event.slug}/"
            )
        with scopes_disabled():
            w = WaitingListEntry.objects.first()
            assert w.item == self.ticket
            via_user: WaitingListEntryViaUser = w.via_user
            self.assert_via_user(via_user)
            return w

    def test_checkout_from_waitinglist(self):
        w = WaitingListEntry.objects.create(
            event=self.event, email="maico@gmail.com", item=self.ticket
        )
        w.via_user = WaitingListEntryViaUser(
            user_id=69,
            email="maico@gmail.com",
            address="Science Park 904",
            city="Amsterdam",
            zip="1098xh",
            member=True,
            favourer=False,
            first_name="Maico",
            last_name="T",
        )
        w.via_user.save()

        with scopes_disabled():
            w.send_voucher()
            assert Voucher.objects.first()

        with requests_mock.Mocker() as m:
            self._setup_mocker_mollie_payment_create(m)
            rv = self.client.get(
                f"/{self.orga.slug}/{self.event.slug}/redeem",
                {"voucher": w.voucher.code},
            )
            assert rv.status_code == 200
            rv = self.client.post(
                f"/{self.orga.slug}/{self.event.slug}/cart/add",
                {
                    "_voucher_code": w.voucher.code,
                    "_voucher_item": f"item_{self.ticket.id}",
                },
            )
            self.assertRedirects(
                rv, f"/{self.orga.slug}/{self.event.slug}/?require_cookie=true"
            )
            self.do_checkout()

        # Validate the order contains via data.
        with scopes_disabled():
            o = Order.objects.first()
            via_user: OrderViaUser = o.via_user
            self.assert_via_user(via_user)
