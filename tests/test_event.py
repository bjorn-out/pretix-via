import hashlib

from django.template.backends.django import Template
from django.template.loader import get_template

from pretix.control.views.main import EventWizard


def test_event_wizard_template_unchanged():
    """
    We hack the event creation wizard such that the subevent option is hidden.

    This tests checks that the original template has not changed during a
    Pretix update, such that we might also need to update our local variant of it.
    """
    original_template: Template = get_template(
        "pretixcontrol/events/create_foundation.html"
    )
    hash = hashlib.md5()
    hash.update(original_template.template.source.encode())
    assert (
        hash.hexdigest() == "6af7d96bf0ee86b9268674f89a60b7a0"
    ), "Pretix template changed, update template pretix_via/create_foundation.html"
    assert EventWizard.templates["foundation"] == "pretix_via/create_foundation.html"
