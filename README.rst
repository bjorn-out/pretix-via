Pretix for via
==========================

This is a plugin for `pretix`_.

Development setup (based on `pretix_cookiecutter`_)
-----------------------------------------------------

0. (Optional) Make sure that you have a working `pretix development setup`_.

1. Clone this repository.

2. Execute ``poetry install`` to install this plugin to a new virtual env.

3. Execute ``poetry shell`` to activate the virtual environment you use for pretix development.

4. (Required when 0. skipped) Execute ``pip install pretix==<version>`` to install pretix to the venv. Find the version from the ``Dockerfile``. We do not use ``poetry`` and ``pyproject.toml`` for the pretix base install, as on production it comes from the docker image.

5. Execute ``make`` within this directory to compile translations.

6. Enable the pretix settings ``export DJANGO_SETTINGS_MODULE=pretix.settings``.

7. Execute all the migrations ``django-admin migrate`` from pretix and this plugins.

8. Start your local pretix server ``django-admin runserver`` and navigate to `localhost`_.

9. You can now use the plugin from this repository for your events by enabling it in the 'plugins' tab in the settings.


License
-------


Copyright 2021 Vereniging Informationwetenschappen Amsterdam

Released under the terms of the Apache License 2.0


.. _localhost: http://localhost:8000
.. _pretix: https://github.com/pretix/pretix
.. _pretix_cookiecutter: https://github.com/pretix/pretix-plugin-cookiecutter
.. _pretix development setup: https://docs.pretix.eu/en/latest/development/setup.html
