FROM pretix/standalone:4.5.0
USER root
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -u && $HOME/.poetry/bin/poetry config virtualenvs.create false
# . is for the installing the pretix_via plugin.
ADD . /pretix_via
ENV DJANGO_SETTINGS_MODULE=
RUN cd /pretix_via \
    && make \
    && $HOME/.poetry/bin/poetry install \
    # TODO Remove if proper fix for SMTP relay available
    #  https://gitlab.com/studieverenigingvia/ict/pretix-via/-/issues/45
    && cat /pretix/src/production_settings.py /pretix_via/pretix_via/deployment/settings.py > /pretix/src/production_settings_via.py
USER pretixuser
ENV DJANGO_SETTINGS_MODULE=production_settings_via
RUN cd /pretix/src && make production
